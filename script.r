#Imports
library(tibble)
library(dplyr)
library(tidyr)
library(reshape2)
library(ggplot2)
require(gridExtra)

setwd(dirname(rstudioapi::getActiveDocumentContext()$path))

# Read FTS CSV data
fts_data <- read.csv(file = 'FTS_Complete.csv')

# Remove unneeded columns.
fts_columns_to_be_removed <- c("Flow.ID",
                               "Original.amount",
                               "Description",
                               "Source.Emergency",
                               "Source.Project",
                               "Source.Plan",
                               "Destination.Plan",
                               "Destination.Emergency",
                               "Destination.Project",
                               "Linked.parent.ID",
                               "Linked.child.ID",
                               "Donor.project.code",
                               "Modality",
                               "Version.ID",
                               "Exchange.rate",
                               "Original.currency",
                               "Amount.shared.on.boundary.(USD)")
fts_data_simplified = fts_data[ , !(names(fts_data) %in% fts_columns_to_be_removed)]

# Only keep donations that went to Yemen.
fts_data_simplified_destinLocYE = subset(fts_data_simplified, 
                                         subset = (Destination.Location == "Yemen"))

# The scope of the study defines the time frame as 2014-2017.
years <- c("2014", "2015", "2016", "2017")
fts_data_simplified_destinLocYE_14_17 = subset(fts_data_simplified_destinLocYE, 
                                         subset = (Destination.Usage.year %in% years))

# Only select actual 'Paid Contributions', not 'Commitments' or 'Pledges'.
fts_data_simplified_destinLocYE_14_17_paidDistribu = subset(fts_data_simplified_destinLocYE_14_17, 
                                                            subset = (Flow.status == "Paid Contribution"))

# Insert a column used for recoding the recipient organizations. This is needed because sometimes organizations have different names within FTS.
fts_data_simplified_destinLocYE_14_17_paidDistribu = add_column(fts_data_simplified_destinLocYE_14_17_paidDistribu, DestinOrgCode = 0, .after = 11)

# Code the organizations within the FTS data.
Coding_Orgs_FTS = function(org_name) {
  if ( org_name == 'United Nations Development Programme' ) {
    return('1')
  } else if ( org_name == 'Action Contre la Faim' ) {
    return('2')
  } else if ( org_name == 'Adventist Development and Relief Agency' ) {
    return('3')
  } else if ( org_name == 'Agency for Technical Cooperation and Development' ) {
    return('4')
  } else if ( org_name == 'Al Khair Relief Development Foundation' ) {
    return('5')
  }  else if ( org_name == 'Al-Badyah Charitable Foundation' ) {
    return('6')
  }  else if ( org_name == 'Al-Baida Coalition for Relief and Humanitarian Actions' ) {
    return('7')
  }  else if ( org_name == 'Benevolence Coalition For Humanitarian Relief' ) {
    return('8')
  }  else if ( org_name == 'The Coalition Of Humanitarian Relief' ) {
    return('9')
  }  else if ( org_name == 'International Committee of the Red Cross' ) {
    return('10')
  }  else if ( org_name == 'International Federation of Red Cross and Red Crescent Societies' ) {
    return('10')
  }  else if ( org_name == 'Norwegian Red Cross' ) {
    return('10')
  }  else if ( org_name == 'Qatar Red Crescent Society' ) {
    return('10')
  }  else if ( org_name == 'Red Crescent Society of the United Arab Emirates' ) {
    return('10')
  }  else if ( org_name == 'Sudanese Red Crescent' ) {
    return('10')
  }  else if ( org_name == 'Yemen Red Crescent Society' ) {
    return('10')
  }  else if ( org_name == 'Kuwait Red Crescent Society' ) {
    return('10')
  }  else if ( org_name == 'CARE International' ) {
    return('11')
  }  else if ( org_name == 'Yemen Humanitarian Fund' ) {
    return('12')
  }  else if ( org_name == 'M├®decins sans Fronti─ìres' ) {
    return('18')
  }  else if ( org_name == 'Enjaz foundation for development' ) {
    return('21')
  }  else if ( org_name == 'Food & Agriculture Organization of the United Nations' ) {
    return('26')
  }  else if ( org_name == 'Global Communities' ) {
    return('28')
  }  else if ( org_name == 'Yemen, Government of' ) {
    return('30')
  }  else if ( org_name == 'Handicap International / Humanity & Inclusion' ) {
    return('32')
  }  else if ( org_name == 'Yemen Higher Relief Committee' ) {
    return('33')
  }  else if ( org_name == 'International Commission for Human Development' ) {
    return('37')
  }  else if ( org_name == 'International Medical Corps' ) {
    return('38')
  }  else if ( org_name == 'International Organization for Migration' ) {
    return('40')
  }  else if ( org_name == 'International Rescue Committee' ) {
    return('42')
  }  else if ( org_name == 'International Islamic Relief Organization' ) {
    return('44')
  }  else if ( org_name == 'Islamic Relief Worldwide' ) {
    return('44')
  }  else if ( org_name == 'Khalifa Bin Zayed Al Nahyan Foundation' ) {
    return('45')
  }  else if ( org_name == 'Mercy Corps' ) {
    return('49')
  }  else if ( org_name == 'Norwegian Refugee Council' ) {
    return('53')
  }  else if ( org_name == 'Office for the Coordination of Humanitarian Affairs' ) {
    return('54')
  }  else if ( org_name == 'OXFAM GB' ) {
    return('59')
  }  else if ( org_name == 'OXFAM Ireland' ) {
    return('59')
  }  else if ( org_name == 'World Food Programme' ) {
    return('64')
  }  else if ( org_name == 'Save the Children' ) {
    return('68')
  }  else if ( org_name == 'Scoop Group' ) {
    return('72')
  }  else if ( org_name == 'United Nations High Commissioner for Refugees' ) {
    return('78')
  }  else if ( org_name == 'United Nations Population Fund' ) {
    return('79')
  }  else if ( org_name == 'United Nations Children\'s Fund' ) {
    return('80')
  }  else if ( org_name == 'World Health Organization' ) {
    return('85')
  }  else if ( org_name == 'Yemeni Social Reform Society' ) {
    return('87')
  }  else if ( org_name == 'Alawn Foundation for Development' ) {
    return('93')
  }  else if ( org_name == 'AlBasar Foundation' ) {
    return('94')
  }  else if ( org_name == 'Alyateem Development Foundation' ) {
    return('95')
  }  else if ( org_name == 'Direct Aid' ) {
    return('96')
  }  else if ( org_name == 'International Campaign to Abolish Nuclear Weapons (ICAN)' ) {
    return('97')
  }  else if ( org_name == 'International Children\'s Action Network' ) {
    return('98')
  }  else if ( org_name == 'International Youth Council - Yemen' ) {
    return('99')
  }  else if ( org_name == 'Japan Platform' ) {
    return('100')
  }  else if ( org_name == 'Japanese NGOs' ) {
    return('101')
  }  else if ( org_name == 'Kuwaiti Yemeni Relief' ) {
    return('102')
  }  else if ( org_name == 'NGOs (details not yet provided)' ) {
    return('91')
  }  else if ( org_name == 'Office of the High Commissioner for Human Rights' ) {
    return('103')
  }  else if ( org_name == 'Physicians Across Continents' ) {
    return('104')
  }  else if ( org_name == 'Private (individuals & organizations)' ) {
    return('63')
  }  else if ( org_name == 'AReform Societ' ) {
    return('87')
  }  else if ( org_name == 'United Nations Office on Drugs and Crime' ) {
    return('105')
  }  else if ( org_name == 'Wethaq Foundation for Civil Orientation' ) {
    return('106')
  }  else if ( org_name == 'World Assembly of Muslim Youth' ) {
    return('107')
  }  else if ( org_name == 'Yemen National Committee for Human Rights' ) {
    return('108')
  }  else if ( org_name == 'Yemeni Association for relief and development' ) {
    return('109')
  }  
}

fts_data_simplified_destinLocYE_14_17_paidDistribu$DestinOrgCode = apply( fts_data_simplified_destinLocYE_14_17_paidDistribu[11], 1, Coding_Orgs_FTS  )

# Format coded orgs into integers and replace NULLS and NAs into 0s.
fts_data_simplified_destinLocYE_14_17_paidDistribu$DestinOrgCode = as.integer(as.character(fts_data_simplified_destinLocYE_14_17_paidDistribu$DestinOrgCode))
fts_data_simplified_destinLocYE_14_17_paidDistribu$DestinOrgCode[fts_data_simplified_destinLocYE_14_17_paidDistribu$DestinOrgCode == "NULL"] <- 0
fts_data_simplified_destinLocYE_14_17_paidDistribu$DestinOrgCode[is.na(fts_data_simplified_destinLocYE_14_17_paidDistribu$DestinOrgCode)] <- 0

# The EU as a donor is only listed in the 'Source Organization' column, not in the 'Source Location' column like all other major donors. Therefor we recode it here.
fts_data_simplified_destinLocYE_14_17_paidDistribu$Source.Location <- as.character(fts_data_simplified_destinLocYE_14_17_paidDistribu$Source.Location)
fts_data_simplified_destinLocYE_14_17_paidDistribu$Source.Organization <- as.character(fts_data_simplified_destinLocYE_14_17_paidDistribu$Source.Organization)
fts_data_simplified_destinLocYE_14_17_paidDistribu = fts_data_simplified_destinLocYE_14_17_paidDistribu %>% 
  mutate(Source.Location = ifelse(Source.Organization == "European Commission", 
                                "EU Institutions", Source.Location))
fts_data_simplified_destinLocYE_14_17_paidDistribu = fts_data_simplified_destinLocYE_14_17_paidDistribu %>% 
  mutate(Source.Location = ifelse(Source.Organization == "European Commission\'s Humanitarian Aid and Civil Protection Department", 
                                  "EU Institutions", Source.Location))

# Create a pivot table to aggregate donation amounts by donor, recipient organization and year.
fts_data_simplified_destinLocYE_14_17_paidDistribu_pivot = dcast(fts_data_simplified_destinLocYE_14_17_paidDistribu, 
                                                           Source.Location + DestinOrgCode ~ Destination.Usage.year, value.var="Amount..USD.", fun.aggregate=sum)

fts_data_simplified_destinLocYE_14_17_paidDistribu_pivot = fts_data_simplified_destinLocYE_14_17_paidDistribu_pivot %>% gather(Year, FTS_Amount, '2014':'2017')


# Read OECD CSV data
oecd_data <- read.csv(file = 'OECD_CompleteHumanitarian.csv')

# Remove unneeded columns.
oecd_columns_to_be_removed <- c("AgencyCode",
                                "Aid_t",
                                "AssocFinance",
                                "Bi_Multi",
                                "Biodiversity",
                                "BudgetIdent",
                                "CRSid",
                                "CapitalExpend",
                                "Category",
                                "ChannelCode",
                                "ClimateAdaptation",
                                "ClimateMitigation",
                                "CommitmentDate",
                                "Commitment_National",
                                "CurrencyCode",
                                "Desertification",
                                "Disbursement_National",
                                "Environment",
                                "ExpectedStartDate",
                                "FTC",
                                "Finance_t",
                                "FlowCode",
                                "Gender",
                                "Geography",
                                "GrantElement",
                                "GrantEquiv",
                                "IncomegroupCode",
                                "IncomegroupName",
                                "InitialReport",
                                "Interest1",
                                "Interest2",
                                "InvestmentProject",
                                "NumberRepayment",
                                "PBA",
                                "ParentChannelCode",
                                "Pdgg",
                                "ProjectNumber",
                                "ProjectTitle",
                                "PurposeName",
                                "RMNCH",
                                "RecipientCode",
                                "RegionCode",
                                "RegionName",
                                "Repaydate1",
                                "Repaydate2",
                                "SectorCode",
                                "Trade",
                                "TypeRepayment",
                                "USD_Adjustment",
                                "USD_Adjustment_Defl",
                                "USD_AmountPartialTied",
                                "USD_AmountPartialTied_Defl",
                                "USD_AmountTied",
                                "USD_AmountUntied",
                                "USD_AmountUntied_Defl",
                                "USD_Amounttied_Defl",
                                "USD_Arrears_Interest",
                                "USD_Arrears_Principal",
                                "USD_Commitment",
                                "USD_Commitment_Defl",
                                "USD_Disbursement_Defl",
                                "USD_Expert_Commitment",
                                "USD_Expert_Extended",
                                "USD_Export_Credit",
                                "USD_Future_DS_Interest",
                                "USD_Future_DS_Principal",
                                "USD_GrantEquiv",
                                "USD_IRTC",
                                "USD_Interest",
                                "USD_Outstanding",
                                "USD_Received",
                                "USD_Received_Defl",
                                "@_id")
oecd_data_simplified = oecd_data[ , !(names(oecd_data) %in% oecd_columns_to_be_removed)]

# Only keep donations that went to Yemen.
oecd_data_simplified_recipientNameYE = subset(oecd_data_simplified, 
                                         subset = (RecipientName == "Yemen"))

# The scope of the study defines the time frame as 2014-2017.
years <- c("2014", "2015", "2016", "2017")
oecd_data_simplified_recipientNameYE_14_17 = subset(oecd_data_simplified_recipientNameYE, 
                                               subset = (Year %in% years))

# Insert two columns used for recoding the recipient organizations. This is needed because sometimes organizations have different names within OECD.
oecd_data_simplified_recipientNameYE_14_17 = add_column(oecd_data_simplified_recipientNameYE_14_17, ChannelNameCode = 0, .after = 2)
oecd_data_simplified_recipientNameYE_14_17 = add_column(oecd_data_simplified_recipientNameYE_14_17, ChannelReportedNameCode = 0, .after = 4)

# Code the organizations within the OECD data.
Coding_Orgs = function(org_name) {
  if ( org_name == '03474 - UNDP (Direct Execution)' ) {
    return('1')
  } else if ( org_name == 'UNDP' ) {
    return('1')
  } else if ( org_name == 'UNDP(United Nations Development Programme)' ) {
    return('1')
  } else if ( org_name == 'United Nations Development Programme' ) {
    return('1')
  } else if ( org_name == 'ACF - Action Against Hunger - France' ) {
    return('2')
  } else if ( org_name == 'ACF International' ) {
    return('2')
  } else if ( org_name == 'ACF/ACTION CONTRE LA FAIM FRANCE' ) {
    return('2')
  } else if ( org_name == 'Action Against Hunger (ACF) - France' ) {
    return('2')
  } else if ( org_name == 'Adventist Development and Relief Agency' ) {
    return('3')
  } else if ( org_name == 'Adventist Development and Relief Agency International' ) {
    return('3')
  } else if ( org_name == 'Agency for Technical Cooperation and Development' ) {
    return('4')
  } else if ( org_name == 'Al - Khair Coalition for Humanitarian Relief' ) {
    return('5')
  } else if ( org_name == 'Al-Badiyh Charitable Foundation' ) {
    return('6')
  } else if ( org_name == 'Al-Baida Coalition for Relief and Humanitarian Actions' ) {
    return('7')
  } else if ( org_name == 'Benevolence Coalition For Humanitarian Relief' ) {
    return('8')
  } else if ( org_name == 'Bollor?Group' ) {
    return('9')
  } else if ( org_name == 'Canadian Red Cross' ) {
    return('10')
  } else if ( org_name == 'Dansk R?de Kors' ) {
    return('10')
  } else if ( org_name == 'Emirates Red Crescent' ) {
    return('10')
  } else if ( org_name == 'Finnish Red Cross' ) {
    return('10')
  } else if ( org_name == 'ICRC' ) {
    return('10')
  } else if ( org_name == 'ICRC - International Committee of the Red Cross' ) {
    return('10')
  } else if ( org_name == 'ICRC - Switzerland' ) {
    return('10')
  } else if ( org_name == 'ICRC(International Committee of the Red Cross)' ) {
    return('10')
  } else if ( org_name == 'ICRC/INTERNATIONAL COMMITTEE OF THE RED CROSS' ) {
    return('10')
  } else if ( org_name == 'International Committee Of The Red Cross' ) {
    return('10')
  } else if ( org_name == 'ICRC/INTERNATIONAL COMMITTEE OF THE RED CROSS/R?A KORSET' ) {
    return('10')
  } else if ( org_name == 'INGO International Committee of the Red Cross ICRC - ICRC - PRT' ) {
    return('10')
  } else if ( org_name == 'International Committee of the Red Cross' ) {
    return('10')
  } else if ( org_name == 'International Committee of the Red Cross (ICRC)' ) {
    return('10')
  } else if ( org_name == 'International Committee of the Red Cross?ICRC)' ) {
    return('10')
  } else if ( org_name == 'International Federation of Red Cross and Red Crescent Societies' ) {
    return('10')
  } else if ( org_name == 'International Red Cross and Red Crescent Societies (ICRC)' ) {
    return('10')
  } else if ( org_name == 'NGO Rode Kruis-Vlaanderen Internationaal' ) {
    return('10')
  } else if ( org_name == 'Norges R?de Kors' ) {
    return('10')
  } else if ( org_name == 'Swedish Red Cross' ) {
    return('10')
  } else if ( org_name == 'CARE Canada' ) {
    return('11')
  } else if ( org_name == 'Care International' ) {
    return('11')
  } else if ( org_name == 'CBPFs(Country-Based Pooled Funds)' ) {
    return('12')
  } else if ( org_name == 'Central Emergency Response Fund' ) {
    return('12')
  } else if ( org_name == 'ERF(Emergency Reponse Fund)' ) {
    return('12')
  } else if ( org_name == 'ERF(Emergency Response Fund)' ) {
    return('12')
  } else if ( org_name == 'UN-led Country-based Pooled Funds' ) {
    return('12')
  } else if ( org_name == 'UNOCHA (Emergency Response Fund)' ) {
    return('12')
  } else if ( org_name == 'Central Government' ) {
    return('0')
  } else if ( org_name == 'Cooperative for Assistance and Relief Everywhere' ) {
    return('15')
  } else if ( org_name == 'Dansk Flygtningehj?lp' ) {
    return('16')
  } else if ( org_name == 'Flyktninghjelpen' ) {
    return('16')
  } else if ( org_name == 'Developing country-based NGO' ) {
    return('0')
  } else if ( org_name == 'Doctors Without Borders' ) {
    return('19')
  } else if ( org_name == 'Donor country-based NGO' ) {
    return('0')
  } else if ( org_name == 'ONG bas? dans un pays donneur' ) {
    return('0')
  } else if ( org_name == 'Donor Government' ) {
    return('0')
  } else if ( org_name == 'Enjaz development foundation' ) {
    return('21')
  } else if ( org_name == 'Enterprise - Non United States Other' ) {
    return('0')
  } else if ( org_name == 'Enterprise - United States Other' ) {
    return('0')
  } else if ( org_name == 'Estonian Rescue Board' ) {
    return('25')
  } else if ( org_name == 'FAO' ) {
    return('26')
  } else if ( org_name == 'Food and Agricultural Organisation' ) {
    return('26')
  } else if ( org_name == 'GIZ' ) {
    return('27')
  } else if ( org_name == 'Global Communities' ) {
    return('28')
  } else if ( org_name == 'Government of the United Arab Emirates' ) {
    return('0')
  } else if ( org_name == 'Government of Yemen' ) {
    return('0')
  } else if ( org_name == 'REPUBLIC OF YEMEN' ) {
    return('0')
  } else if ( org_name == 'Yemen Government' ) {
    return('0')
  } else if ( org_name == 'Governorate in Yemen' ) {
    return('0')
  } else if ( org_name == 'Handicap International Canada' ) {
    return('32')
  } else if ( org_name == 'High relief committee (Republic of yemen)' ) {
    return('33')
  } else if ( org_name == 'ILO' ) {
    return('34')
  } else if ( org_name == 'International Labour Organisation - Regular Budget Supplementary Account' ) {
    return('34')
  } else if ( org_name == 'Information Management and Mine Action Programs' ) {
    return('35')
  } else if ( org_name == 'International Business and Technical Consultants Inc.' ) {
    return('36')
  } else if ( org_name == 'International Commission for Human Development' ) {
    return('37')
  } else if ( org_name == 'International Medical Corps' ) {
    return('38')
  } else if ( org_name == 'International NGO' ) {
    return('0')
  } else if ( org_name == 'ONG INTERNATIONALE' ) {
    return('0')
  } else if ( org_name == 'International Organisation for Migration' ) {
    return('40')
  } else if ( org_name == 'International Organisation for Migration (IOM)' ) {
    return('40')
  } else if ( org_name == 'International Organization for Migration' ) {
    return('40')
  } else if ( org_name == 'IOM' ) {
    return('40')
  } else if ( org_name == 'IOM - International Organisation for Migration/Organizzazione internazionale per l emigrazione' ) {
    return('40')
  } else if ( org_name == 'IOM(International Organisation for Migration )' ) {
    return('40')
  } else if ( org_name == 'IOM(International Organisation for Migration)' ) {
    return('40')
  } else if ( org_name == 'IOM(International Organization for Migration)' ) {
    return('40')
  } else if ( org_name == 'IOM/INTERNATIONAL ORGANIZATION FOR MIGRATION)' ) {
    return('40')
  } else if ( org_name == 'International Relief and Development' ) {
    return('41')
  } else if ( org_name == 'International Rescue Committee' ) {
    return('42')
  } else if ( org_name == 'IRC/INTERNATIONAL RESCUE COMMITTEE' ) {
    return('42')
  } else if ( org_name == 'INTERSOS' ) {
    return('43')
  } else if ( org_name == 'ISLAMIC RELIEF' ) {
    return('44')
  } else if ( org_name == 'Islamic Relief Canada' ) {
    return('44')
  } else if ( org_name == 'Islamic Relief Sweden' ) {
    return('44')
  } else if ( org_name == 'Islamic Relief Worldwide' ) {
    return('44')
  } else if ( org_name == 'khalifa Bin Zayed Al Nahyan Foundation' ) {
    return('45')
  } else if ( org_name == 'King Salman for Relief and Humanitarian Aid Center' ) {
    return('46')
  } else if ( org_name == 'KOICA') {
    return('47')
  } else if ( org_name == 'Matrix International Logistics Inc.') {
    return('48')
  } else if ( org_name == 'Mercy Corps') {
    return('49')
  } else if ( org_name == 'Multilateral Organisations') {
    return('0')
  } else if ( org_name == 'Network') {
    return('0')
  } else if ( org_name == 'Networks') {
    return('0')
  } else if ( org_name == 'Non-Governmental Organisation (NGO) and Civil Society') {
    return('0')
  } else if ( org_name == 'NON-GOVERNMENTAL ORGANISATIONS (NGOs) AND CIVIL SOCIETY') {
    return('0')
  } else if ( org_name == 'Norwegian Refugee Council') {
    return('53')
  } else if ( org_name == 'NRC/NORWEGIAN REFUGEE COUNCIL') {
    return('53')
  } else if ( org_name == 'OCHA - Office for the Coordination of Humanitarian Affairs') {
    return('54')
  } else if ( org_name == 'OCHA - United Nations Office for the Coordination of Humanitarian Affairs') {
    return('54')
  } else if ( org_name == 'Un Ocha') {
    return('54')
  } else if ( org_name == 'UN Office for the Coordination of Humanitarian Aff') {
    return('54')
  } else if ( org_name == 'UN UNOCHA - Office of Co-ordination of Humanitarian Affairs - PRT') {
    return('54')
  } else if ( org_name == 'United Nations Office of Co-ordination of Humanitarian Affairs') {
    return('54')
  } else if ( org_name == 'United Nations Office of Co-ordination of Humanitarian Affairs (UNOCHA)') {
    return('54')
  } else if ( org_name == 'United nations Office of co-ordination of Humanitarian Affairs') {
    return('54')
  } else if ( org_name == 'UNOCHA') {
    return('54')
  } else if ( org_name == 'UN OCHA') {
    return('54')
  } else if ( org_name == 'UNOCHA - Switzerland') {
    return('54')
  } else if ( org_name == 'UNOCHA - UN Office of Co-ordination of Humanitarian Affairs') {
    return('54')
  } else if ( org_name == 'UNOCHA office in Yemen') {
    return('54')
  } else if ( org_name == 'UNOCHA/UNITED NATIONS OFFICE FOR THE COORDINATION OF HUMANITARIAN AFFAIRS') {
    return('54')
  } else if ( org_name == 'Other') {
    return('0')
  } else if ( org_name == 'Other non-financial corporations') {
    return('0')
  } else if ( org_name == 'Other public entities in donor country') {
    return('0')
  } else if ( org_name == 'Other public entities in recipient country') {
    return('0')
  } else if ( org_name == 'Oxfam') {
    return('59')
  } else if ( org_name == 'OXFAM - provider country office') {
    return('59')
  } else if ( org_name == 'Oxfam Deutschland e. V. (OXFAM - provider country office)') {
    return('59')
  } else if ( org_name == 'OXFAM International') {
    return('59')
  } else if ( org_name == 'Oxfam-Qu?ec') {
    return('59')
  } else if ( org_name == 'Private sector in provider country') {
    return('0')
  } else if ( org_name == 'Private sector in recipient country') {
    return('0')
  } else if ( org_name == 'Private sector in third country') {
    return('0')
  } else if ( org_name == 'Private sector institution') {
    return('0')
  } else if ( org_name == 'Private Sector Institutions') {
    return('0')
  } else if ( org_name == 'Programme alimentaire mondial') {
    return('64')
  } else if ( org_name == 'UN WFP - UNHAS Humanitarian Air Service - PRT') {
    return('64')
  } else if ( org_name == 'UN World Food Programme WFP_PAM - WFP - PRT') {
    return('64')
  } else if ( org_name == 'United Nations World Food Programme (UN WFP)') {
    return('64')
  } else if ( org_name == 'United Nations World Food Programme (WFP)') {
    return('64')
  } else if ( org_name == 'WFP') {
    return('64')
  } else if ( org_name == 'WFP - World Food Programme') {
    return('64')
  } else if ( org_name == 'WFP(World Food Programme)') {
    return('64')
  } else if ( org_name == 'WFP/WORLD FOOD PROGRAMME') {
    return('64')
  } else if ( org_name == 'World Food Programme') {
    return('64')
  } else if ( org_name == 'WFP - WORLD FOOD PROGRAMME') {
    return('64')
  } else if ( org_name == 'World Food programme (WFP)') {
    return('64')
  } else if ( org_name == 'World Food Programme (WFP)') {
    return('64')
  } else if ( org_name == 'Public corporations') {
    return('0')
  } else if ( org_name == 'Public-Private Partnerships (PPP)') {
    return('0')
  } else if ( org_name == 'Recipient Government') {
    return('0')
  } else if ( org_name == 'Red Barnet') {
    return('68')
  } else if ( org_name == 'Save the Children') {
    return('68')
  } else if ( org_name == 'Save the Children - donor country office') {
    return('68')
  } else if ( org_name == 'Save the Children Canada') {
    return('68')
  } else if ( org_name == 'Save the Children Deutschland e. V. (provider country office)') {
    return('68')
  } else if ( org_name == 'Save the Children Federation Inc.') {
    return('68')
  } else if ( org_name == 'Relief Coalition in Aden') {
    return('69')
  } else if ( org_name == 'SANA\'A MUNICIPALITY') {
    return('0')
  } else if ( org_name == 'Scoop group') {
    return('72')
  } else if ( org_name == 'Socotra Development Association') {
    return('73')
  } else if ( org_name == 'Solidarios sin Fronteras') {
    return('74')
  } else if ( org_name == 'SVENSKA MISSIONSR?DET') {
    return('75')
  } else if ( org_name == 'Third Country Government (Delegated co-operation)') {
    return('0')
  } else if ( org_name == 'UN Office of the High Commissioner for Refugees UNHCR_HCR - UNHCR - PRT') {
    return('78')
  } else if ( org_name == 'UNHCR') {
    return('78')
  } else if ( org_name == 'UNHCR - U.N. Office of the United Nations High Commissioner for Refugees') {
    return('78')
  } else if ( org_name == 'UNHCR - UN Office of the UN High Commissioner for Refugees') {
    return('78')
  } else if ( org_name == 'UNHCR - United Nations High Commissioner for Refugees') {
    return('78')
  } else if ( org_name == 'UNHCR(United Nations Office of the United Nations High Commissioner for Refugees)') {
    return('78')
  } else if ( org_name == 'UNHCR/UNITED NATIONS HIGH COMMISSIONER FOR REFUGEES') {
    return('78')
  } else if ( org_name == 'United Nations Office of the United Nations High Commissioner for Refugees') {
    return('78')
  } else if ( org_name == 'United Nations Office of the United Nations High Commissioner for Refugees (UNHCR)') {
    return('78')
  } else if ( org_name == 'UNFPA') {
    return('79')
  } else if ( org_name == 'UNFPA - United Nations Population Fund') {
    return('79')
  } else if ( org_name == 'United Nations Population Fund') {
    return('79')
  } else if ( org_name == 'UNICEF') {
    return('80')
  } else if ( org_name == 'UNICEF - UNITED NATIONS CHILDREN S FUND') {
    return('80')
  } else if ( org_name == 'UNICEF - Childrens Fund - PRT') {
    return('80')
  } else if ( org_name == 'UNICEF - United Nations Children s Fund') {
    return('80')
  } else if ( org_name == 'UNICEF - United Nations Children\'s Fund') {
    return('80')
  } else if ( org_name == 'UNICEF(United Nations Children`s Fund)') {
    return('80')
  } else if ( org_name == 'UNICEF(United Nations Children\'s Fund)') {
    return('80')
  } else if ( org_name == 'UNICEF/UNITED NATIONS CHILDRENS FUND') {
    return('80')
  } else if ( org_name == 'United Nations Children?s Fund') {
    return('80')
  } else if ( org_name == 'United Nations Childrens Fund') {
    return('80')
  } else if ( org_name == 'United Nations Children\'s Fund') {
    return('80')
  } else if ( org_name == 'United Nations Children\'s Fund (UNICEF)') {
    return('80')
  } else if ( org_name == 'United Nations Office for Project Services') {
    return('81')
  } else if ( org_name == 'University college or other teaching institution research institute or think?tank') {
    return('0')
  } else if ( org_name == 'University college or other teaching institution research institute or think-tank') {
    return('0')
  } else if ( org_name == 'Unspecified Vendors - Bosnia and Herzegovina') {
    return('0')
  } else if ( org_name == 'Watan Foundation') {
    return('84')
  } else if ( org_name == 'WHO - World Health Organization') {
    return('85')
  } else if ( org_name == 'WHO(World Health Organization)') {
    return('85')
  } else if ( org_name == 'World Health Organisation - assessed contributions') {
    return('85')
  } else if ( org_name == 'World Health Organisation - core voluntary contributions account') {
    return('85')
  } else if ( org_name == 'World Health Organisation (WHO)') {
    return('85')
  } else if ( org_name == 'World Health Organization') {
    return('85')
  } else if ( org_name == 'World Health Organization (WHO)') {
    return('85')
  } else if ( org_name == 'Yemen Development Network for NGOs') {
    return('86')
  } else if ( org_name == 'Yemeni Development Network For NGOs') {
    return('86')
  } else if ( org_name == 'Yemeni Social Reform Society') {
    return('87')
  } else if ( org_name == 'MSB/MYNDIGHETEN F? SAMH?LLSSKYDD & BEREDSKAP') {
    return('88')
  } else if ( org_name == 'United Nations (UN) agency fund or commission') {
    return('0')
  } else if ( org_name == 'United Nations agency fund or commission (UN)') {
    return('0')
  } else if ( org_name == 'Personal Services Contractors - USAID') {
    return('0')
  } else if ( org_name == 'NGO-s') {
    return('0')
  } else if ( org_name == '03469 - National Execution') {
    return('0') 
  }
}

oecd_data_simplified_recipientNameYE_14_17$ChannelNameCode = apply( oecd_data_simplified_recipientNameYE_14_17[2], 1, Coding_Orgs  )
oecd_data_simplified_recipientNameYE_14_17$ChannelReportedNameCode = apply( oecd_data_simplified_recipientNameYE_14_17[4], 1, Coding_Orgs  )

# As we now have two columns with coded organizations, we need to create one that combines both.
oecd_data_simplified_recipientNameYE_14_17 = add_column(oecd_data_simplified_recipientNameYE_14_17, CodedOrg = 0, .after = 17)
oecd_data_simplified_recipientNameYE_14_17$CodedOrg = as.integer(as.character(oecd_data_simplified_recipientNameYE_14_17$CodedOrg))

oecd_data_simplified_recipientNameYE_14_17$ChannelNameCode = as.integer(as.character(oecd_data_simplified_recipientNameYE_14_17$ChannelNameCode))
oecd_data_simplified_recipientNameYE_14_17$ChannelNameCode[oecd_data_simplified_recipientNameYE_14_17$ChannelNameCode == "NULL"] <- 0
oecd_data_simplified_recipientNameYE_14_17$ChannelNameCode[is.na(oecd_data_simplified_recipientNameYE_14_17$ChannelNameCode)] <- 0

oecd_data_simplified_recipientNameYE_14_17$ChannelReportedNameCode = as.integer(as.character(oecd_data_simplified_recipientNameYE_14_17$ChannelReportedNameCode))
oecd_data_simplified_recipientNameYE_14_17$ChannelReportedNameCode[oecd_data_simplified_recipientNameYE_14_17$ChannelReportedNameCode == "NULL"] <- 0
oecd_data_simplified_recipientNameYE_14_17$ChannelReportedNameCode[is.na(oecd_data_simplified_recipientNameYE_14_17$ChannelReportedNameCode)] <- 0

oecd_data_simplified_recipientNameYE_14_17$CodedOrg <- ifelse(oecd_data_simplified_recipientNameYE_14_17$ChannelNameCode > 0, oecd_data_simplified_recipientNameYE_14_17$ChannelNameCode, 
                                                              ifelse(oecd_data_simplified_recipientNameYE_14_17$ChannelReportedNameCode > 0 , oecd_data_simplified_recipientNameYE_14_17$ChannelReportedNameCode,0
                                                              ))


# Remove some erroneous data that Excel inserted into the CSV.
oecd_data_simplified_recipientNameYE_14_17$USD_Disbursement = as.double(as.character(oecd_data_simplified_recipientNameYE_14_17$USD_Disbursement))

isna <- is.na(oecd_data_simplified_recipientNameYE_14_17)
cols <- match(c("USD_Disbursement"), colnames(oecd_data_simplified_recipientNameYE_14_17))
iscol <- col(isna) %in% cols
oecd_data_simplified_recipientNameYE_14_17[isna & iscol] <- 0

# Within the OECD data, amounts are reported as 1/1000000. To make OECD amounts comparable with FTS amounts we multiply OECD amounts by 1000000.
oecd_data_simplified_recipientNameYE_14_17$USD_Disbursement = oecd_data_simplified_recipientNameYE_14_17$USD_Disbursement * 1000000

# Create a pivot table to aggregate donation amounts by donor, recipient organization and year.
oecd_data_simplified_recipientNameYE_14_17_pivot = dcast(oecd_data_simplified_recipientNameYE_14_17, 
                                                                 DonorName + CodedOrg ~ Year, value.var="USD_Disbursement", fun.aggregate=sum)
oecd_data_simplified_recipientNameYE_14_17_pivot = oecd_data_simplified_recipientNameYE_14_17_pivot %>% gather(Year, OECD_Amount, '2014':'2017')


# Rename columns to make merging easier.
oecd_data_simplified_recipientNameYE_14_17_pivot = oecd_data_simplified_recipientNameYE_14_17_pivot %>% 
  rename(
    Donor = DonorName,
    Organization = CodedOrg
  )
fts_data_simplified_destinLocYE_14_17_paidDistribu_pivot = fts_data_simplified_destinLocYE_14_17_paidDistribu_pivot %>% 
  rename(
    Donor = Source.Location,
    Organization = DestinOrgCode
  )
oecd_fts_merge = merge(x = oecd_data_simplified_recipientNameYE_14_17_pivot, y = fts_data_simplified_destinLocYE_14_17_paidDistribu_pivot, by=c("Donor","Organization","Year"), all = TRUE)

# Remove rows with NA or 0 values for (FTS & OECD) amounts
oecd_fts_merge = subset(oecd_fts_merge, subset = !(is.na(oecd_fts_merge$OECD_Amount) & is.na(oecd_fts_merge$FTS_Amount))
                                              & !(oecd_fts_merge$OECD_Amount == 0.0 & oecd_fts_merge$FTS_Amount == 0) 
)
oecd_fts_merge[is.na(oecd_fts_merge)] <- 0


#Only select Grand Bargain signatory donors and organizations in the merged data.
oecd_fts_merge$Organization = as.character(oecd_fts_merge$Organization)
GBSorgs = c('26', '10', '40', '80', '1', '78', '79', '54', '64')
GBSdonors = c('Australia', 'Belgium', 'Bulgaria', 'Canada', 'Czech Republic', 'Denmark', 'EU Institutions', 'Germany', 'Italy', 'Japan', 'Luxembourg', 'Netherlands', 'Norway', 'Poland', 'Sweden', 'Switzerland', 'United Kingdom', 'United States')
oecd_fts_merge_onlyGBSdonors = oecd_fts_merge[oecd_fts_merge$Donor %in% GBSdonors,]
oecd_fts_merge_onlyGBSdonorsAndGBSorgs = oecd_fts_merge_onlyGBSdonors[oecd_fts_merge_onlyGBSdonors$Organization %in% GBSorgs,]

#Check the distribution of reported FTS and OECD amounts.
p1 <- ggplot(oecd_fts_merge_onlyGBSdonorsAndGBSorgs, aes(y=oecd_fts_merge_onlyGBSdonorsAndGBSorgs$OECD_Amount)) + 
  geom_boxplot()
p2 <- ggplot(oecd_fts_merge_onlyGBSdonorsAndGBSorgs, aes(y=oecd_fts_merge_onlyGBSdonorsAndGBSorgs$FTS_Amount)) + 
  geom_boxplot()
grid.arrange(p1, p2, ncol=2)


# Remove outliers
#outliers_OECD = boxplot(oecd_fts_merge_onlyGBSdonorsAndGBSorgs$OECD_Amount)$out
#outliers_FTS = boxplot(oecd_fts_merge_onlyGBSdonorsAndGBSorgs$FTS_Amount)$out

lowerq_oecd = quantile(oecd_fts_merge_onlyGBSdonorsAndGBSorgs$OECD_Amount)[2]
upperq_oecd = quantile(oecd_fts_merge_onlyGBSdonorsAndGBSorgs$OECD_Amount)[4]
lowerq_fts = quantile(oecd_fts_merge_onlyGBSdonorsAndGBSorgs$FTS_Amount)[2]
upperq_fts = quantile(oecd_fts_merge_onlyGBSdonorsAndGBSorgs$FTS_Amount)[4]

iqr_oecd = upperq_oecd - lowerq_oecd
iqr_fts = upperq_fts - lowerq_fts

mild.threshold.upper_oecd = (iqr_oecd * 1.5) + upperq_oecd
mild.threshold.lower_oecd = lowerq_oecd - (iqr_oecd * 1.5)
mild.threshold.upper_fts = (iqr_fts * 1.5) + upperq_fts
mild.threshold.lower_fts = lowerq_fts - (iqr_fts * 1.5)

extreme.threshold.upper_oecd = (iqr_oecd * 3) + upperq_oecd
extreme.threshold.lower_oecd = lowerq_oecd - (iqr_oecd * 3)
extreme.threshold.upper_fts = (iqr_fts * 3) + upperq_fts
extreme.threshold.lower_fts = lowerq_fts - (iqr_fts * 3)

oecd_fts_merge_onlyGBSdonorsAndGBSorgs_exclOutliers = subset(oecd_fts_merge_onlyGBSdonorsAndGBSorgs, 
              subset = (!(OECD_Amount > extreme.threshold.upper_oecd | OECD_Amount < extreme.threshold.lower_oecd)))
oecd_fts_merge_onlyGBSdonorsAndGBSorgs_exclOutliers = subset(oecd_fts_merge_onlyGBSdonorsAndGBSorgs_exclOutliers, 
              subset = (!(FTS_Amount > extreme.threshold.upper_fts | FTS_Amount < extreme.threshold.lower_fts)))

#oecd_fts_merge_onlyGBSdonorsAndGBSorgs[which(oecd_fts_merge_onlyGBSdonorsAndGBSorgs$OECD_Amount %in% outliers_OECD),]
#oecd_fts_merge_onlyGBSdonorsAndGBSorgs[which(oecd_fts_merge_onlyGBSdonorsAndGBSorgs$FTS_Amount %in% outliers_FTS),]

#oecd_fts_merge_onlyGBSdonorsAndGBSorgs_exclOutliers <- oecd_fts_merge_onlyGBSdonorsAndGBSorgs[-which(oecd_fts_merge_onlyGBSdonorsAndGBSorgs$OECD_Amount %in% outliers_OECD),]
#oecd_fts_merge_onlyGBSdonorsAndGBSorgs_exclOutliers <- oecd_fts_merge_onlyGBSdonorsAndGBSorgs[-which(oecd_fts_merge_onlyGBSdonorsAndGBSorgs$FTS_Amount %in% outliers_FTS),]

#Sums of all FTS and OECD amounts, per year.
oecd_fts_merge_onlyGBSdonorsAndGBSorgs_sums = oecd_fts_merge_onlyGBSdonorsAndGBSorgs %>%
  group_by(oecd_fts_merge_onlyGBSdonorsAndGBSorgs$Year) %>%
  summarise_if(is.numeric, sum, na.rm = TRUE)
oecd_fts_merge_onlyGBSdonorsAndGBSorgs_exclOutliers_sums = oecd_fts_merge_onlyGBSdonorsAndGBSorgs_exclOutliers %>%
  group_by(oecd_fts_merge_onlyGBSdonorsAndGBSorgs_exclOutliers$Year) %>%
  summarise_if(is.numeric, sum, na.rm = TRUE)

# Means of all FTS and OECD amounts, per year.
oecd_fts_merge_onlyGBSdonorsAndGBSorgs_means = oecd_fts_merge_onlyGBSdonorsAndGBSorgs %>%
  group_by(oecd_fts_merge_onlyGBSdonorsAndGBSorgs$Year) %>%
  summarise_if(is.numeric, mean, na.rm = TRUE)
oecd_fts_merge_onlyGBSdonorsAndGBSorgs_exclOutliers_means = oecd_fts_merge_onlyGBSdonorsAndGBSorgs_exclOutliers %>%
  group_by(oecd_fts_merge_onlyGBSdonorsAndGBSorgs_exclOutliers$Year) %>%
  summarise_if(is.numeric, mean, na.rm = TRUE)


#Plot sums and means of FTS and OECD data
ggplot(oecd_fts_merge_onlyGBSdonorsAndGBSorgs_exclOutliers_sums, aes(x=oecd_fts_merge_onlyGBSdonorsAndGBSorgs_exclOutliers_sums$`oecd_fts_merge_onlyGBSdonorsAndGBSorgs_exclOutliers$Year`, group = 1)) + 
  geom_line(aes(y = oecd_fts_merge_onlyGBSdonorsAndGBSorgs_exclOutliers_sums$OECD_Amount), color = "red") + 
  geom_line(aes(y = oecd_fts_merge_onlyGBSdonorsAndGBSorgs_exclOutliers_sums$FTS_Amount), color="blue") 

ggplot(oecd_fts_merge_onlyGBSdonorsAndGBSorgs_exclOutliers_means, aes(x=oecd_fts_merge_onlyGBSdonorsAndGBSorgs_exclOutliers_means$`oecd_fts_merge_onlyGBSdonorsAndGBSorgs_exclOutliers$Year`, group = 1)) + 
  geom_line(aes(y = oecd_fts_merge_onlyGBSdonorsAndGBSorgs_exclOutliers_means$OECD_Amount), color = "red") + 
  geom_line(aes(y = oecd_fts_merge_onlyGBSdonorsAndGBSorgs_exclOutliers_means$FTS_Amount), color="blue") 

ggplot(oecd_fts_merge_onlyGBSdonorsAndGBSorgs_sums, aes(x=oecd_fts_merge_onlyGBSdonorsAndGBSorgs_sums$`oecd_fts_merge_onlyGBSdonorsAndGBSorgs$Year`, group = 1)) + 
  geom_line(aes(y = oecd_fts_merge_onlyGBSdonorsAndGBSorgs_sums$OECD_Amount), color = "red") + 
  geom_line(aes(y = oecd_fts_merge_onlyGBSdonorsAndGBSorgs_sums$FTS_Amount), color="blue") 

ggplot(oecd_fts_merge_onlyGBSdonorsAndGBSorgs_means, aes(x=oecd_fts_merge_onlyGBSdonorsAndGBSorgs_means$`oecd_fts_merge_onlyGBSdonorsAndGBSorgs$Year`, group = 1)) + 
  geom_line(aes(y = oecd_fts_merge_onlyGBSdonorsAndGBSorgs_means$OECD_Amount), color = "red") + 
  geom_line(aes(y = oecd_fts_merge_onlyGBSdonorsAndGBSorgs_means$FTS_Amount), color="blue") 


# 2014-2015 vs. 2016-2017
# Copy reported amounts from both timeframes into separate dataframes.
preGB = c('2014', '2015')
postGB = c('2016', '2017')
oecd_fts_merge_onlyGBSdonorsAndGBSorgs_exclOutliers_preGB = oecd_fts_merge_onlyGBSdonorsAndGBSorgs_exclOutliers[oecd_fts_merge_onlyGBSdonorsAndGBSorgs_exclOutliers$Year %in% preGB,]
oecd_fts_merge_onlyGBSdonorsAndGBSorgs_exclOutliers_postGB = oecd_fts_merge_onlyGBSdonorsAndGBSorgs_exclOutliers[oecd_fts_merge_onlyGBSdonorsAndGBSorgs_exclOutliers$Year %in% postGB,]
oecd_fts_merge_onlyGBSdonorsAndGBSorgs_preGB = oecd_fts_merge_onlyGBSdonorsAndGBSorgs[oecd_fts_merge_onlyGBSdonorsAndGBSorgs$Year %in% preGB,]
oecd_fts_merge_onlyGBSdonorsAndGBSorgs_postGB = oecd_fts_merge_onlyGBSdonorsAndGBSorgs[oecd_fts_merge_onlyGBSdonorsAndGBSorgs$Year %in% postGB,]


# Wilcoxon signed rank tests
# Excluding outliers
# 2014-2015, excluding outliers.
wilcox.test(oecd_fts_merge_onlyGBSdonorsAndGBSorgs_exclOutliers_preGB$FTS_Amount, 
            oecd_fts_merge_onlyGBSdonorsAndGBSorgs_exclOutliers_preGB$OECD_Amount, alternative = "two.sided")


# 2016-2017, excluding outliers.
wilcox.test(oecd_fts_merge_onlyGBSdonorsAndGBSorgs_exclOutliers_postGB$OECD_Amount, 
            oecd_fts_merge_onlyGBSdonorsAndGBSorgs_exclOutliers_postGB$FTS_Amount, alternative = "two.sided")
# 2014-2017, excluding outliers.
wilcox.test(oecd_fts_merge_onlyGBSdonorsAndGBSorgs_exclOutliers$OECD_Amount, 
            oecd_fts_merge_onlyGBSdonorsAndGBSorgs_exclOutliers$FTS_Amount, alternative = "two.sided")

# Including outliers (full sample)
# 2014-2015, including outliers.
wilcox.test(oecd_fts_merge_onlyGBSdonorsAndGBSorgs_preGB$OECD_Amount, 
            oecd_fts_merge_onlyGBSdonorsAndGBSorgs_preGB$FTS_Amount, alternative = "two.sided")
# 2016-2017, including outliers.
wilcox.test(oecd_fts_merge_onlyGBSdonorsAndGBSorgs_postGB$OECD_Amount, 
            oecd_fts_merge_onlyGBSdonorsAndGBSorgs_postGB$FTS_Amount, alternative = "two.sided")
# 2014-2017, including outliers.
wilcox.test(oecd_fts_merge_onlyGBSdonorsAndGBSorgs$OECD_Amount, 
            oecd_fts_merge_onlyGBSdonorsAndGBSorgs$FTS_Amount, alternative = "two.sided")


